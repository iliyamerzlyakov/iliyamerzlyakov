package ru.mim.patient;

/**
 * Класс для
 *
 * @author Merzlyakov I.
 */
@SuppressWarnings("all")
public class Main {
    public static void main(String[] args) {
        Patient patient1 = new Patient("Пациент 1",1997, 138, true);
        Patient patient2 = new Patient("Пациент 2",1995, 256, false);
        Patient patient3 = new Patient("Пациент 3",1989, 560, true);
        Patient patient4 = new Patient("Пациент 4",1987, 116, false);
        Patient patient5 = new Patient("Пациент 5",2003, 998, true);

        Patient[] patients = {patient1, patient2, patient3, patient4, patient5};
        dispPat(patients);
    }

    /**
     * Ищет и выводит массив пациентов, прошедших диспанцеризацию до 2000 года
     *
     * @param patients массив пациентов
     */

    private static void dispPat(Patient[] patients) {
        System.out.println("Пациенты: ");
        int pat = patients.length;
        for (int i = 0; i < pat; i++) {
            if (patients[i].isDispensary() && patients[i].getBirthday() < 2000) {
                System.out.println("Пациент{" +
                        "Фамилия='" + patients[i].getSurname() + '\'' +
                        ", Год рождения=" + patients[i].getBirthday() +
                        ", Номер карточки=" + patients[i].getCard() +
                        ", Диспанцеризация пройдена" +
                        '}');
            }
        }
    }
}
