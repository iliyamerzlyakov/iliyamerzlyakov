package ru.mim.patient;

/**
 * Класс для
 *
 * @author  Merzlyakov I.
 */
@SuppressWarnings("all")
public class Patient {

    /**
     * Фамилия пациента
     */
    private String surname;
    /**
     * Год рождения пациента
     */
    private int birthday;
    /**
     * Номер карточки
     */
    private int card;
    /**
     * Диспанцеризация пациента
     */
    private boolean dispensary;

    /**
     *
     *
     * @param surname
     * @param birthday
     * @param card
     * @param dispensary
     */

    public Patient(String surname, int birthday, int card, boolean dispensary) {
        this.birthday = birthday;
        this.card = card;
        this.dispensary = dispensary;
        this.surname = surname;
    }
    @SuppressWarnings("unused")
    public Patient() {
        this("не указана", 0, 0, false);
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthday() {
        return birthday;
    }

    public int getCard() {
        return card;
    }

    public boolean isDispensary() {
        return dispensary;
    }

    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая фамилию, год рождения, номер карточки и информацию о диспанцеризации
     */

    @Override
    public String toString() {
        return "Patient{" +
                "surname='" + surname + '\'' +
                ", birthday=" + birthday +
                ", card=" + card +
                ", dispensary=" + dispensary +
                '}';
    }
}