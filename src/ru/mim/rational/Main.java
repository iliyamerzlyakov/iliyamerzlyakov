package ru.mim.rational;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите дробь вида a/b знак a/b");
        String denNDiv = scanner.nextLine();
        Rational[] rationals = input(denNDiv);
        operation(denNDiv, rationals);
    }

    private static Rational[] input(String denNDiv) {
        String[] withoutSpace = denNDiv.replaceAll("[\\s +:*-]", "/").split("/");

        int a = Integer.parseInt(withoutSpace[0]);
        int b = Integer.parseInt(withoutSpace[1]);
        Rational firstNumb = new Rational(a, b);

        int c = Integer.parseInt(withoutSpace[4]);
        int d = Integer.parseInt(withoutSpace[5]);
        Rational secondNum = new Rational(c, d);

        Rational[] rationals = {firstNumb, secondNum};
        return rationals;

    }

    private static void operation(String denNDiv, Rational[] rationals) {
        String[] withoutSpace = denNDiv.split("\\s");
        String operation = withoutSpace[1];
        Rational numb;
        switch (operation) {
            case "+":
                numb = Rational.plus(rationals);
                System.out.println(numb.getDenominator()+"/"+numb.getDivider());
                break;
            case "-":
                numb = Rational.mines(rationals);
                System.out.println(numb.getDenominator()+"/"+numb.getDivider());
                break;
            case "*":
                numb = Rational.mult(rationals);
                System.out.println(numb.getDenominator()+"/"+numb.getDivider());
                break;
            case ":":
                numb = Rational.div(rationals);
                System.out.println(numb.getDenominator()+"/"+numb.getDivider());
                break;
            default:
                System.out.println("Неверный знак операций");
                break;
        }
    }
}
