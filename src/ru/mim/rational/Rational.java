package ru.mim.rational;

public class Rational {
    private int denominator;
    private int divider;


    public Rational(int denominator, int divider) {
        this.denominator = denominator;
        this.divider = divider;
    }

    public Rational(Object input) {
        this(1, 1);
    }

    @Override
    public String toString() {
        return "Rational{" +
                "denominator=" + denominator +
                ", divider=" + divider +
                '}';
    }

    static Rational mult(Rational[] rationals) {
        int denominator = rationals[0].denominator * rationals[1].denominator;
        int deviver = rationals[0].divider * rationals[1].divider;
        return new Rational(denominator, deviver).raw();
    }

    static Rational div(Rational[] rationals) {
        int denominator = rationals[0].denominator * rationals[1].divider;
        int deviver = rationals[1].denominator * rationals[0].divider;
        return new Rational(denominator, deviver).raw();
    }

    static int lcm(Rational[] rationals) {
        int a = rationals[0].divider;
        int b = rationals[1].divider;
        return a*b/gcd(rationals);
    }
    static int gcd(Rational[] rationals){
        int a = rationals[0].divider;
        int b = rationals[1].divider;
        return b ==0 ? a : a%b;
    }

    static Rational plus(Rational[] rationals) {
        int a = rationals[0].denominator;
        int b = rationals[1].divider;
        int denominator = 0;

        if (a>b){
            denominator = a + b * gcd(rationals);
        } else {
            denominator = b + a * gcd(rationals);
        }

        int deviver = lcm(rationals);
        return new Rational(denominator, deviver);
    }

    static Rational mines(Rational[] rationals) {
        int a = rationals[0].denominator;
        int b = rationals[1].divider;
        int denominator = 0;

        if (a>b){
            denominator = a + b * gcd(rationals);
        } else {
            denominator = b + a * gcd(rationals);
        }

        int deviver = lcm(rationals);
        return new Rational(denominator, deviver).raw();
    }

    Rational raw() {
        int a = denominator;
        int b = divider;

        if (a > b && a % b == 0) {
            a /= b;
            b /= b;
        }
        if (b > a && b % a == 0) {
            b /= a;
            a /= a;
        }
        if (a > b && a % b != 0) {
            for (int i = 1; i <= a; i++) {
                if (a % i == 0) {
                    while (b > 3) {
                        a /= i;
                        b /= i;
                        i++;
                    }
                }
            }
        }

        if (b > a && b % a != 0) {
            for (int i = 1; i <= b; i++) {
                a/=i;
                b/=i;
                for (int j = 1; j ==b ; j++) {
                    while (b%i!=0){
                        a/=j;
                        b/=j;
                    }
                }
            }
        }
        return new Rational(a, b);

    }

    public int getDenominator() {
        return denominator;
    }

    public int getDivider() {
        return divider;
    }
}
