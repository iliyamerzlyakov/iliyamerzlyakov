package ru.mim.college;

public class Student {
    private int year;
    private String specialty;

    public Student(int year, String specialty) {
        this.year = year;
        this.specialty = specialty;
    }

    public int getYear() {
        return year;
    }

    public String getSpecialty() {
        return specialty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "год поступления=" + year +
                ", специальность='" + specialty + '\'' +
                '}';
    }
}
