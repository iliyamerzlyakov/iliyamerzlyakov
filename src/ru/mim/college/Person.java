package ru.mim.college;

public class Person {
    private static String name;
    private static String gender;

    public Person(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    public static String getName() {
        return name;
    }

    public static String getGender() {
        return gender;
    }

}
