package ru.mim.calculator;

public class MathInt {
    public static int sum(int x, int y) {
        return (x + y);
    }

    public static int del(int x, int y) {
        return (x - y);
    }

    public static int multiplication(int x, int y) {
        return (x * y);
    }

    public static double divison(double x, double y) {
        return (x / y);
    }

    public static int remains(int x, int y) {
        return (x % y);
    }

    public static double pow(int x, int y) {
        if (y == 0) {
            return 1;
        }
        double result = x;
        if (y < 0){
            result = 1.0/(result*(-y));
        }else {
            for (int i = 1; i < y; i++) {
                result = result * x;
            }
        }
        return result;
    }
}
