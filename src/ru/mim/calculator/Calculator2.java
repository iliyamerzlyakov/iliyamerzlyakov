package ru.mim.calculator;

import java.util.Scanner;
public class Calculator2 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String expression;
        int firstNumber, secondNumber;
        System.out.println("введите выражение -");
        expression = scanner.nextLine();


        String[] variables = expression.split(" ");
        firstNumber = Integer.valueOf(variables[0]);
        secondNumber = Integer.valueOf(variables[2]);

        switch (variables[1]) {
            case "+":
                System.out.println(firstNumber + "+" + secondNumber + "=" + MathInt.sum(firstNumber, secondNumber));
                break;
            case "-":
                System.out.println(firstNumber + "-" + secondNumber + "=" + MathInt.del(firstNumber, secondNumber));
                break;
            case "*":
                System.out.println(firstNumber + "*" + secondNumber + "=" + MathInt.multiplication(firstNumber, secondNumber));
                break;
            case "/":
                System.out.println(firstNumber + "/" + secondNumber + "=" + MathInt.divison(firstNumber, secondNumber));
                break;
            case "%":
                System.out.println(firstNumber + "%" + secondNumber + "=" + MathInt.remains(firstNumber, secondNumber));
                break;
            case "^":
                System.out.println(firstNumber + "^" + secondNumber + "=" + MathInt.pow(firstNumber, secondNumber));
                break;
        }
    }
}

