package ru.mim.calculator;

import java.util.Scanner;

public class Calculator {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int x;
        int y;
        System.out.print("Введите первое целое число:");
        x = scanner.nextInt();
        System.out.print("Введите второе целое число:");
        y = scanner.nextInt();
        System.out.println("Cумма чисел "+ x + " и " + y + " = " + MathInt.Sum(x,y));
        System.out.println("Разность чисел "+ x + " и " + y + " = " + MathInt.Del(x,y));
        System.out.println("Умножение чисел "+ x + " и " + y + " = " + MathInt.Multiplication(x,y));
        System.out.println("Целочисленное деление чисел "+ x + " и " + y + " = " + MathInt.Divison(x,y));
        System.out.println("Остаток от деления чисел "+ x + " и " + y + " = " + MathInt.Remains(x,y));
        System.out.println("Введение числа "+ x + " в степень " + y + " = " + MathInt.Pow(x,y));
    }

    public static class MathInt {

        private static int Sum(int x, int y) {
            return (x + y);
        }

        private static int Del(int x, int y) {
            return (x - y);
        }

        private static int Multiplication(int x, int y) {
            return (x * y);
        }

        private static int Divison(int x, int y) {
            return (x / y);
        }

        private static int Remains(int x, int y) {
            return (x % y);
        }

        private static int Pow(int x, int y) {
            if (y == 0) {
                return 1;
            }
            int result = x;
            for (int i = 1; i < y; i++) {
                result = result * x;
            }
            return result;
        }
    }
}