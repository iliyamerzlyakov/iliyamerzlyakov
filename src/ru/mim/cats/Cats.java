package ru.mim.cats;

public class Cats {
    private String name;
    private int year;
    private Gender gender;

    public Cats(String name, Gender male, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Cats{" +
                "Кличка = '" + name + '\'' +
                ", Год рождения = " + year +
                '}';
    }
}
