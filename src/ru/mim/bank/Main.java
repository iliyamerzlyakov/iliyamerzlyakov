package ru.mim.bank;

/**Класс для реализации действий с банком
 *
 * @author Merzlyakov I.I.
 */
public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank(156,12);
        System.out.println(bank);
        System.out.printf("Состояние счета с учетом начислений %% за один год: %.2f",bank.balanceWithPercent());
    }
}

