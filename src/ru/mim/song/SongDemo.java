package ru.mim.song;

public class SongDemo {

    public SongDemo() {

    }

    /**
     * Класс для работы с объектами класса Baggage
     *
     * @author Merzlyakov I.
     */

    public static void main(String[] args) {
        Song yesterday  = new Song("War Of Change", "Thousand Foot Krutch", 90);
        Song smell = new Song("Smells Like Teen Spirit", "Nirvana ", 223);
        Song queen = new Song("Monster", "Skilet", 342);
        Song planes = new Song("Get Out Alive", "Three Days Grace", 51);
        Song sna = new Song("One Too Many", "Three Days Grace ", 90);

        Song[] box = {yesterday, smell, queen, planes, sna};
        songs(box);

        /**
         * Цикл, позволяющий вывести на экран схожи ли последние песни или несхожи
         *
         * @param ru.mim.box массив песен
         */

        for (int i = 0; i < box.length; i++) {
            if (box[0].isSameCategory(box[box.length - 1])) {
                System.out.println("Первая и последня песни - схожи");
                break;
            } else {
                System.out.println("Несхожи");
            }
        }
    }
    /**
     * Метод, позволяющий вывести на экран песни категории short
     *
     * @param box массив песен
     */

    private static void songs(Song box[]) {
        int c = Song.getDuration();
        if (c < 120) {
            for (Song song : box) {
                if (song.category().equals("short")) {
                    System.out.println(song + "" + "short");
                    return;

                }

            }
        }
    }
}





