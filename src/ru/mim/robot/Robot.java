package ru.mim.robot;

import ru.mim.robot.Direction;

public class Robot {
    private int x;
    private int y;
    private Direction direction;

    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Robot(int x, int y){
        this(x,y,Direction.UP);

    }
    public void turnRight (){
        switch (direction){
            case UP:
                direction = Direction.RIGHT;
                break;
            case DOWM:
                direction = Direction.DOWM;
                break;
            case LEFT:
                direction = Direction.LEFT;
                break;
            case RIGHT:
                direction = Direction.RIGHT;
                break;
        }
    }

    public void move(int endX, int endY) {
        if (endX>x){
            while (direction!=Direction.RIGHT){
                turnRight();
            }
            while (x!=endX){
                oneStep();
            }
        }
        if (endX<x){
            while (direction!=Direction.LEFT){
                turnRight();
            }
            while (x!=endX){
                oneStep();
            }
        }
        if (endY>y){
            while (direction!=Direction.UP){
                turnRight();
            }
            while (y!=endY){
                oneStep();
            }
        }
        if (endY<y){
            while (direction!=Direction.DOWM){
                turnRight();
            }
            while (y!=endY){
                oneStep();
            }
        }
    }

    private void oneStep() {
        switch (direction){
            case RIGHT:
                x++;
                break;
            case LEFT:
                x--;
                break;
            case DOWM:
                y--;
                break;
            case UP:
                y++;
                break;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }
}
