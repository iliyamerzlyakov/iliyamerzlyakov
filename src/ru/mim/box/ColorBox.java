package ru.mim.box;

public class ColorBox extends Box {
    private Color color;

    public ColorBox(String name, int width, int height, int depth, Color color, Heavy heavy) {
        super(name, width, height, depth);
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "ru.mim.box.ColorBox{" +
                super.toString() +
                "color=" + color +
                '}';
    }
}
