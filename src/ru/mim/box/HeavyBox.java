package ru.mim.box;

public class HeavyBox extends ColorBox {

    private double weight;
    private Heavy heavy;

    public HeavyBox(String name, int width, int height, int depth, Color color, Heavy heavy) {
        super(name, width, height, depth, color, heavy);
        this.heavy = heavy;
    }



    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "ru.mim.box.HeavyBox{" +
                super.toString() +
                "heavy=" + heavy +
                '}';
    }
    
}

