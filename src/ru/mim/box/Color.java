package ru.mim.box;

/**
 * Список цветов коробки:
 * <ul>
 * <li>белый</li>
 * <li>жёлтый</li>
 * <li>красный</li>
 * <li>голубой</li>
 * <li>чёрный</li>
 * <li>зелёный</li>
 * </ul>
 *
 * @author Т.А. Инюшкина
 */
public enum Color {
    WHITE,
    YELLOW,
    RED,
    BLUE,
    BLACK,
    GREEN
}
