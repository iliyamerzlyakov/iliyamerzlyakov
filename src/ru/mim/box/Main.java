package ru.mim.box;

public class Main {
    public static void main(String[] args) {
        Box launchBox = new Box( "ланчбокс",10, 6, 12);
        Box safe = new Box("сейф", 200, 100, 150);
        Box standardBox = new Box();
        Box cubeBox = new Box("кубик", 5);
        ColorBox colorBox =
                new ColorBox("Цветная",
                        4,6,8, Color.YELLOW, Heavy.VERYHEAVY);
        HeavyBox heavyBox =
                new HeavyBox("Тяжелая",
                        5, 7, 9, Color.RED, Heavy.HEAVY);
        System.out.println("Тяжелая" + heavyBox);
        System.out.println("Первая цветная" + colorBox);
        System.out.println("Ланч-бокс: " + launchBox
                + ", объем = " + launchBox.volume());
        System.out.println("Сейф: " + safe
                + ", объем = " + safe.volume());
        System.out.println("Стандартная: " + standardBox);
        System.out.println("Кубик: " + cubeBox);
        Box[] cubes = {launchBox, safe, standardBox, cubeBox};
        outArrayOfBox(cubes);
        int maxCube = maxBox(cubes);
        System.out.println("Максимальный объем коробки: "
                + maxCube);
    }

    private static void outArrayOfBox(Box[] cubes) {
        for (int i = 0; i < cubes.length; i++) {
            System.out.println((i + 1) + " коробочка " + cubes[i]);

        }
    }

    /**
     * Метод определяет максимальный объем коробки
     * из массива коробок(boxes)
     *
     * @param boxes Массив коробок
     * @return Максимальный объем коробки
     */
    private static int maxBox(Box[] boxes) {
        int max = boxes[0].volume();
        for (int i = 1; i < boxes.length; i++) {
            int volume = boxes[i].volume();
            if (volume > max) {
                max = volume;
            }
        }
        return max;
    }

}
